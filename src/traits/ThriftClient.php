<?php
/**
 * Created by PhpStorm.
 * User: craig
 * Date: 2/26/14
 * Time: 9:02 AM
 */

namespace sz\envoy\thrift\traits;

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\TBufferedTransport;
use Thrift\Transport\THttpClient;
use Thrift\Transport\TSocket;

trait ThriftClient {
    protected $transport;
    protected $config;

    /** @var int how long to wait for the request in milliseconds */
    public $timeout;

    /**
     * Creates a new thrift service client
     *
     * Example $config object:
     * <code>
     * $config = {
     *     host: '127.0.0.1',
     *     port: 9091,
     *     transportType: 'socket',
     *     uri: '/path/to/destination'
     * };
     * </code>
     *
     * NOTE: $config->uri is only used when $config->transportType = 'http'
     *
     * @param $config the configuration options for the thrift service client
     */
    public function construct($config)
    {
        $this->config = $config;
        $this->transport = null;
        $this->timeout = 0;
    }

    protected function openTransport()
    {
        switch ($this->config->transportType) {
            case 'http':
                $socket = new THttpClient($this->config->host, $this->config->port, $this->config->uri);
                break;

            case 'socket':
            default:
                $socket = new TSocket($this->config->host, $this->config->port);
                break;
        }

        if ($this->timeout > 0) {
            $socket->setRecvTimeout($this->timeout);
        }

        $this->transport = new TBufferedTransport($socket, 1024, 1024);
        $this->input_ = new TBinaryProtocol($this->transport);
        $this->output_ = $this->input_;
        $this->transport->open();
    }

    protected function closeTransport()
    {
        $this->transport->close();
    }
} 