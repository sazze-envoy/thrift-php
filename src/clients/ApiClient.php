<?php
/**
 * Defines the Api class
 *
 * PHP version 5
 *
 * @author Craig Thayer <cthayer@sazze.com>
 * @copyright 2014 Sazze, Inc.
 */

namespace sz\envoy\thrift\clients;

use sz\envoy\thrift\en\emitir\ApiServiceClient;
use sz\envoy\thrift\traits\ThriftClient;

class ApiClient extends ApiServiceClient
{
    use ThriftClient;

    public function api($request)
    {
        $this->openTransport();

        $ret = parent::api($request);

        $this->closeTransport();

        return $ret;
    }
} 