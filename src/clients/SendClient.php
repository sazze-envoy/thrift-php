<?php
/**
 * Defines the Send class
 *
 * PHP version 5
 *
 * @author Craig Thayer <cthayer@sazze.com>
 * @copyright 2014 Sazze, Inc.
 */

namespace sz\envoy\thrift\clients;

use sz\envoy\thrift\en\emitir\SendServiceClient;
use sz\envoy\thrift\traits\ThriftClient;

class SendClient extends SendServiceClient
{
    use ThriftClient;

    public function __construct($config)
    {
        $this->construct($config);
    }

    public function send($request) {
        $this->openTransport();

        $ret = parent::send($request);

        $this->closeTransport();

        return $ret;
    }
} 